#include<stdio.h>
#include<stdlib.h>

float celsius2fahrenheit(float temp) {
    return temp*9/5+32 ;
}

float fahrenheit2celsius(float temp) {
    return (temp-32)*5/9;
}

int main() {
    char choix ;
    float temp;
    
    choix = ' '; 
    while (choix != '1' && choix != '2') {
        printf("(1) Celsius vers Fahrenheit ou (2) Fahrenheit vers Celsius ? ");
        scanf(" %c", &choix);
    }
    switch (choix) {
        case '1':
            printf("Température en Celsius : ");
            scanf("%f", &temp);
            printf("En Fahrenheit : %.2f\n", celsius2fahrenheit(temp));
            break;
        case '2':
            printf("Température en Fahrenheit : ");
            scanf("%f", &temp);
            printf("En Celsius : %.2f\n", fahrenheit2celsius(temp));
            break;
    }
    exit(0);
}


