#include <stdio.h>
#include<stdlib.h>

int count_char(const char *str, char c) {
    int count = 0;
    while (*str != '\0') {
        if (*str == c) {
            count++;
        }
        str++;
    }
    return count;
}

int main(int argc, char *argv[]) {

    char character = argv[1][0]; 
    const char *str = argv[2]; 

    int occurrence = count_char(str, character);

    printf("%d \n",occurrence);
    
    exit(0);
    }
